import click

from .genre import *

@click.group()
def cli():
    pass


@cli.command()
@click.argument('files', nargs=-1)
def show(files):
    for file in files:
        genres_list = list_genres(file)
        click.echo("::".join(genres_list))


@cli.command()
@click.argument('genre', nargs=1)
@click.argument('files', nargs=-1)
def delete(genre, files):
    for file in files:
        delete_genre(genre, file)
        genres_list = list_genres(file)
        click.echo("::".join(genres_list))


@cli.command()
@click.argument('genre', nargs=1)
@click.argument('files', nargs=-1)
def add(genre, files):
    for file in files:
        add_genre(genre, file)
        genres_list = list_genres(file)
        click.echo("::".join(genres_list))


@cli.command()
@click.argument('old_genre', nargs=1)
@click.argument('new_genre', nargs=1)
@click.argument('files', nargs=-1)
def replace(old_genre, new_genre, files):
    for file in files:
        if old_genre in list_genres(file):
            delete_genre(old_genre, file)
            add_genre(new_genre, file)
            
            genres_list = list_genres(file)
            click.echo("::".join(genres_list))
        else:
            click.echo(f"{old_genre} not in {file}")


if __name__ == "__main__":
    cli()
