import re
import eyed3

def list_genres(file):
    track = eyed3.load(file)
    genres_list = re.split('[;:,] *', str(track.tag.genre))
    return genres_list


def delete_genre(genre, file):
    track = eyed3.load(file)
    genres_list = list_genres(file)
    try:
        genres_list.remove(genre)
        track.tag.genre = ";".join(genres_list)
        track.tag.save()
    except ValueError:
        print(f"{genre} not found in {genres_list}")
    return genres_list


def add_genre(genre, file):
    track = eyed3.load(file)
    genres_list = sorted([genre, *list_genres(file)])

    track.tag.genre = ";".join(genres_list)
    track.tag.save()

    return genres_list


def set_genres(genres_list, file):
    track = eyed3.load(file)

    track.tag.genre = ";".join(genres_list)
    track.tag.save()

    return genres_list


replace_genres = set_genres
